export default {
  background: '#e0e9e1',
  dark: '#0b110e',
  orange: '#fc8e09',
  green: '#506257',
};
